# DKX/Docker/gcloud

gcloud in docker

## Usage

```bash
$ docker run -v `pwd`/certificate.json:/certs/certificate.json registry.gitlab.com/dkx/docker/gcloud gcloud projects list
```
