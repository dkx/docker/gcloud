ARG GCLOUD_VERSION

FROM google/cloud-sdk:${GCLOUD_VERSION}-alpine

COPY config /root/.config/gcloud
COPY docker-entrypoint.sh /docker-entrypoint.sh

RUN mkdir /certs && \
	chmod +x /docker-entrypoint.sh && \
	gcloud components install --quiet beta

ENTRYPOINT ["/docker-entrypoint.sh"]
