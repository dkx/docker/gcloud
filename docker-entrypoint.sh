#!/usr/bin/env sh


set -e


GCP_CERTIFICATE_FILE="/certs/certificate.json"


if [[ \( ! -z "${GCP_CERTIFICATE}" \) -o \( ! -f "${GCP_CERTIFICATE_FILE}" \) ]]; then
	echo "${GCP_CERTIFICATE}" > "${GCP_CERTIFICATE_FILE}"
fi


if [[ -f "${GCP_CERTIFICATE_FILE}" ]]; then
	gcloud auth activate-service-account --key-file="${GCP_CERTIFICATE_FILE}"
fi

exec "$@"
